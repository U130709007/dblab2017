create schema if not exists bank;
use bank;

drop table if exists branch;
create table branch (
	branch_name varchar(12) not null,
    branch_city varchar(12) not null,
    assets int,
    primary key (branch_name)
);
drop table if exists account;
create table account (
  account_number varchar(5) not null,
  branch_name varchar(12),
  balance int not null,
  primary key (account_number)
);

